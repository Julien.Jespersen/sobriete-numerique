# Sobriété numérique
## Le streaming ou le téléchargement de vidéos de cours universitaires (i.e. MOOCs)
---

## Problème
La surenchère de cours filmés et distribués implique de plus en plus d'infrastructure (serveurs) et de plus en plsu de terminaux performant pour assurer la diffusion et consommation de ces médias.

Souvent la valeur ajoutée de l'image animée est quasi nulle pédagiquement parlant, seul le contenu textuel est réellement utile. (lien vers un exemple) Ce dernier peut être transmis sous forme de chaîne de caractères (.txt, .html, .rtf …) ou de son (.wav, .mp3 …)

## UNe solution
- Sensibiliser les profs à ne pas systèmatiquement se filmer mais uniquement s'enregistrer (audio)
- Construire et mettre à dispo un prog permettant de facilement faire une sorte de slideshow combinant un fichier audio et des images fixes apparaissant au moment opportunt.

## ressources
- https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API
- https://mdn.github.io/webaudio-examples/output-timestamp/
- https://www.unige.ch/enseignement-a-distance/maitriser-outils/capsules-video/
- https://www.unige.ch/enseignement-a-distance/files/9515/9197/4229/PPT_1_Enregistrer_son_cours_avec_PowerPoint.pdf
- https://www.greenit.fr/2019/07/22/%ef%bb%bfvideo-en-ligne-quels-impacts-environnementaux/
- https://www.colincrawley.com/audio-file-size-calculator/



## autre piste: tuto pour les enseignants
Les enseignants ont tendance à simplement s'enregistrer, audio et vidéo, avec leurs dias puis de mettre le fichier vidéo (i.e. .mp4) à dispo sur le média serveur de l'Uni. (https://mediaserver.unige.ch/). On estime qu'une vidéo de 45 min. pèse env. 2 Go.
Il est facilement possible d'enregistrer la présentation Powerpoint incluant la piste audio. Ce fichier fait env. 12 Mo. 160 fois moins lourd.
Page help pour exporter en vidéo ou en fichier "sobre" (.ppsx ou .pptx) [https://support.microsoft.com/en-us/office/turn-your-presentation-into-a-video-c140551f-cb37-4818-b5d4-3e30815c3e83]

## Pour les profs
### Qualité pédagogique
La qualité pédagogique n'est pas réduite si on ne voit pas, en permanence, le conférencier.

L'impact environnemental est grandement amélioré si le conférencier renonce à se filmer. Dans certains cas de voir le conférencier fait totalement sens. Dans d'autres cela n'apporte quasiment rien.


